/*global
    alert
*/
function getPrices() {
  return {
    articles: [1800, 2500, 4500],
    radioOps: {
      op1: 200,
      op2: 100,
      op3: 300,
    },
    checkOps: {
      check1: 200,
      check2: 400,
    }
  };
}

function updatePrice() {
    
  let s = document.getElementsByName("articles");
  let select = s[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.articles[priceIndex];
  }

  let radioDiv = document.getElementById("radios");

  let checkDiv = document.getElementById("checkboxes");
  
  switch(select.value) { 
  
  case "1": 
  { radioDiv.style.display = "none"; checkDiv.style.display = "none"; break;} 
  case "2": 
  { radioDiv.style.display = "none"; checkDiv.style.display = "block"; break;} 
  case "3": 
  { radioDiv.style.display = "block"; checkDiv.style.display = "none"; break;} 
  }
  
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.checkOps[checkbox.name];
      if (propPrice !== undefined) {
        price += propPrice;
      }
    }
  });
  
   let radios = document.getElementsByName("radioOps");
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.radioOps[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  });
  
  let r = document.getElementById("articlePrice");
  let number = document.getElementById("number").value;
  if (!(/^[1-9][0-9]*$/).test(number)) {
      r.innerHTML = "Неверное количество!";
      return;
  }
  number = parseInt(number);
  r.innerHTML = "Стоимость: " + price * number;
}

window.addEventListener('DOMContentLoaded', function (event) {

  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  

  let s = document.getElementsByName("articles");
  let select = s[0];

  select.addEventListener("change", function(event) {

    updatePrice();
  });
  

  let radios = document.getElementsByName("radioOps");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {

      updatePrice();
    });
  });


  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      updatePrice();
    });
  });
  let number = document.getElementById("number");
  number.addEventListener("change", updatePrice);

  updatePrice();
});

